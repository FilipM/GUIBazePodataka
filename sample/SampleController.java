package sample;

import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

/**
 * Created by filip on 28.06.15..
 */
public class SampleController {

    public TextField textField;
    public Button buttonTest;

    public void displayOnMouseClicked(Event event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "You typed: " + textField.getText(), ButtonType.OK);
        alert.showAndWait();

        if(alert.getResult() == ButtonType.OK){
            return;
        }
    }
}
