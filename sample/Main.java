package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        try{
            Main.stage = stage;
            stage.setResizable(false);
            stage.setTitle("Cool program");
            BorderPane root = FXMLLoader.load(getClass().getResource("sample.fxml"));
            Scene scene = new Scene(root, 287, 287);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

            stage.setScene(scene);
            stage.show();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
